class SessionsController < ApplicationController
  before_action :check_if_logged_in?, except: [:destroy]

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user&.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash.now[:notice] = 'Logged in successfully'
      redirect_to user
    else
      flash.now[:alert] = 'There was something wrong with your login credentials.'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = 'Logged out'
    redirect_to root_path
  end

  private

  def check_if_logged_in?
    return unless logged_in?

    flash[:alert] = 'You are already logged in.'
    redirect_to root_path
  end
end
