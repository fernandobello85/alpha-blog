class Category < ApplicationRecord
  has_many :article_categories
  has_many :articles, through: :article_categories
  has_one_attached :image
  validates :name, presence: true, uniqueness: { case_sensitive: false }, length: { minimum: 3, maximum: 25 }
  validate :acceptable_image

  private

  def acceptable_image
    return unless image.attached?

    unless image.byte_size <= 5.megabytes
      errors.add(:image, 'is too big. Your image must be 5 megabytes maximum.')
    end

    acceptable_types = ['image/jpeg', 'image/png']
    unless acceptable_types.include?(image.content_type)
      errors.add(:image, 'must be a JPEG or PNG')
    end
  end
end
