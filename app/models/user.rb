class User < ApplicationRecord
  attr_accessor :is_admin_applying_update
  has_many :articles, dependent: :destroy
  has_one_attached :avatar
  validates :username, presence: true, uniqueness: { case_sensitive: false }, length: { minimum: 3, maximum: 25 }
  VALID_EMAIL_REGEX = /\A[^@\s]+@([^@\s]+\.)+[^@\W]+\z/
  validates :email, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 105 }, format: { with: VALID_EMAIL_REGEX }
  validates :password, presence: true, length: { minimum: 6 }, unless: :is_admin_applying_update
  validate :acceptable_image
  before_save { self.email = email.downcase }
  has_secure_password

  private

  def acceptable_image
    return unless avatar.attached?

    unless avatar.byte_size <= 5.megabytes
      errors.add(:avatar, 'is too big. Your image must be 5 megabytes maximum.')
    end

    acceptable_types = ['image/jpeg', 'image/png']
    unless acceptable_types.include?(avatar.content_type)
      errors.add(:avatar, 'must be a JPEG or PNG')
    end
  end
end
