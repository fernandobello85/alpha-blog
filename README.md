# README

This Ruby on Rails web application is an exercise from the RoR web developer course of Udemy.

Nomada Blog allows signed up users to create articles with associated categories and navigate through the other user's articles. 
Guests users can see articles and categories but they can't create articles.

Things you may want to cover:

* Ruby version ruby 2.6.3
* Rails version Rails 6.0.3.2
* DB: PostgreSQL RDBMS

Dependencies : 
* [Bootstrap 4.5 ]
* [Active Storage to manipulate attached files]
* [AWS for store files in production]

Deployment : 
Heroku [Nomada Blog](https://nomada-blog.herokuapp.com)


~~~sh
bundle

rails db:drop db:create db:migrate db:seed

rails s
~~~

## Command de test:
~~~
rails test (in the root directory)
~~~

