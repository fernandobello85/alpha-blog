require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  setup do
    @category = Category.new(name: 'Sport')
  end

  test 'category should be valid' do
    assert @category.valid?
  end

  test 'name should be present' do
    @category.name = ' '
    assert_not @category.valid?
    @category.name = 'Sport'
    assert @category.valid?
  end

  test 'name should be unique' do
    @category.save!
    @second_category = Category.new(name: 'Sport')
    assert_not @second_category.valid?
    @second_category = Category.new(name: 'Sport 2')
    assert @second_category.valid?
  end

  test 'name should not be too long' do
    @category.name = 'Sport' * 6
    assert_not @category.valid?
  end

  test 'name should not be too short' do
    @category.name = 'Sp'
    assert_not @category.valid?
  end
end
