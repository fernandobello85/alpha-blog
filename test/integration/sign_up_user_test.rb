require 'test_helper'

class SignUpUserTest < ActionDispatch::IntegrationTest

  test 'should sign up the user' do
    get '/signup'
    assert_response :success
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { username: 'Bob', email: 'bob@email_test.com', password: 'password123' } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
    assert_match 'Welcome Bob to the Nomada Blog, you have successfully signed up.', response.body
  end
end
