require 'test_helper'

class CreateArticleTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username: 'Jhon', email: 'jhon@nomada_blog.com', password: 'pswd123')
    @category = Category.create(name: 'Sports')
    sign_in_as(@user)
  end

  test 'should create a new article' do
    get '/articles/new'
    assert_response :success
    assert_difference 'Article.count', 1 do
      post articles_path, params: { article: { title: 'Test article', description: 'Some description for test article', category_ids: [Category.last.id] } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
    assert_match 'Test article', response.body
    assert_match 'Some description for test article', response.body
  end
end
